--=============================================================================
--== Name       :  ipb_slave_tbw                                             ==
--== Author     :  Luis Alberto Perez Moreno                                 ==
--== Start Data :  September 30th 2019                                       ==
--== Description:  ONU Phase scan  w.r.t  OLT  Clock  domain                 ==
--==              The test bench create  ONU  Tx and  Toggle date in  ONU    ==
--==              clock domain,  OLT Tx strobe is created in different clock ==
--==              domain. The ONU clock is shifting time to time to create   ==
--                jitter and verify the correct behavior of the module       ==
--                Including assertion stage  to search inconsistencies such as ==
--                a bad result from  "Phase Scan"  Overlapping in SET Phase   ==
--                procedure.                                                 ==
--=============================================================================
--== Simulation :  VHDL ver 2008                                             ==
--=============================================================================
 --==         System  Library                          ==
 --======================================================
 library  IEEE; 
 use ieee.std_logic_1164.all; 
-----------------------------------------
 use ieee.numeric_std.all; 
 use ieee.std_logic_unsigned.all; 
 --! Specific packages 
 use work.ipbus_reg_types.all;  -- IPbus_reg
--------------------------------------------------------

 library modelsim_lib;
   use modelsim_lib.util.all;
  
  library work;
  use     work.all; 
 --=========================================================  
 --=======================================================
 --==       Custom Libraries  and package              ==
 --=======================================================
   --use work.ipbus.all; 
   --use work.system_package.all; 
   --use work.ipbus_decode_ltu_logic.all;               -- Comment:   IPbus  address table 
 -- use work.pon_olt_package.all;
 ---------------------------------------------------------
 ----  S I M U L A T I O N    L I B R A R I E S           --
 ---------------------------------------------------------- 
   use work.package_tb.all; 
 --   S T D I O   L I B   (Read & Write File)          --
 --------------------------------------------------------

   Library std; 
    use std.textio.all; 
  ----------------------------------------------------------
  --========================================================
  --==  E N T I T Y    D E C L A R A T I O N              ==
  --========================================================
   ENTITY  ipb_slave_tbw is 
            end  ipb_slave_tbw; 
  --========================================================
  --==           A R C H I T E C T U R E                  ==
  --========================================================
 architecture ipb_slave_tbw_beha  of ipb_slave_tbw is 
 -- -----------------------------------------------------
 --    Generic    --
 -- -----------------------------------------------------
    constant period_strobe_t      : integer := 6 ;    -- Comment 1 BC  w.r.t 240 MHz. 

---  Signals 
   signal   insert_reg_input_t    : boolean := true ; 

   signal  ctrl_reg_i_t           : ipb_reg_v(0 downto 0) := (others => (others => '0'));
   signal  status_reg_o_t         : ipb_reg_v(0 downto 0); 
   
   signal  clk_a_i_t              : std_logic := '0';
   signal  data_a_i_t             : std_logic_vector (199 downto 0) :=(others => '0');		
   signal  strobe_a_i_t      : std_logic := '0';

   
     -- CTP  OLT TX trobe aligned to ONU clcok 
    signal strobe_b_i_t   :   std_logic := '0'; 
   signal  clk_b_i_t                 : std_logic := '0';

   signal  data_b_o_t                : std_logic_vector (199 downto 0) := (others => '0'); 
   signal  strobe_b_o_t              : std_logic := '0'; 
   

    signal clk_tst                 : std_logic := '0'; 
    signal phase_test              : time  := 0 ns; 

    signal  HIGH_TIME   : time  := 0 ns ;

     -- Comment:  Spy  signals. 
     signal   set_phase_selected_p :  std_logic := '0'; 
     signal   active_phase_scan_s  :  std_logic := '0'; 
     signal   strobe_in_cc_s       :  integer range 0 to 7 := 0; 
     signal   meas_strobe_b_o_s    :  std_logic := '0'; 
     signal   set_phase_s          :  integer := 0; 
     ------------------------------------------- 
     --     C L O C K    S I G N A L S        --
     --------------------------------------------
     constant  ttc_period            : time := 4.1667 ns;       -- Comment: Frequency 31.25 MHz,  
     -- Constant. 
      constant c_clock_period  : time  := (ttc_period * 6); 
     
    ---------------------------------------------------------------------
    -- ---------- A u x i l i a r y  S i g n a l s  ----------------------    
    ----------------------------------------------------------------------
      signal  enable_clocks: std_logic:= '1' ;
     
      signal  report_signal: std_logic := '0'; 
     
     signal  olt_clock_active : std_logic := '1'; 

     signal clk_comp_t : std_logic := '0';

     signal  FREQ    : real := 240000000.0 ; 
     signal  PHASE   : time := 0 ns; 
     signal  PHASE_o :time := 0 ns;
     signal  run     :std_logic := '1';  

   
    ------------------------------------------------
    --     TEST ACTIVE                            --
    ------------------------------------------------
     constant  active_s :boolean := true;  
  ------------------------------------------------------
  --  PROCEDUREs 
  ------------------------------------------------------
  -- =====================
  -- ==    START        ==
  -- =====================
      procedure start_cmd ( signal ctrl_reg_s : out ipb_reg_v (0 downto  0))
             is
        begin 
          wait until (rising_edge (clk_b_i_t));  -- Comment:  Aligned with clk_b_i_t by IPb Sync_reg  
          ctrl_reg_s(0)(0)  <= '1'; 
             report "PHASE  SCAN START"; 
      end start_cmd; 
  -- ====================
  -- ===    STOP      ===
  -- ====================
    procedure stop_cmd ( signal ctrl_reg_s : out ipb_reg_v (0 downto  0))
      is
     begin 
       wait until (rising_edge (clk_b_i_t));  -- Comment:  Aligned with clk_b_i_t by IPb Sync_reg  
       ctrl_reg_s(0)(0)  <= '0'; 
      report " STOP PHASE SCAN"; 
    end stop_cmd; 
   -- ==================== 
   -- ==    SET Phase  ===
   -- ====================
    procedure  set_phase_cmd (constant  c_pahase_value : in  integer;
                          signal    ctrl_reg_s     : out ipb_reg_v (0 downto  0))
              is 
      variable  phase_select_v : std_logic_vector (2 downto 0) := (others => '0'); 
         begin 
            phase_select_v := std_logic_vector(to_unsigned (c_pahase_value,phase_select_v'length)); 
            wait until (rising_edge (clk_b_i_t)); 
            ctrl_reg_s(0)(5 downto 1) <= phase_select_v & "01"; 
            wait until (rising_edge (clk_b_i_t)); 
            ctrl_reg_s(0)(5 downto 1) <= phase_select_v & "00"; 
            report " SET PHASE ";
        end set_phase_cmd;
    -----------------------------------------------------
    begin --   B E G I N   A R C H I T E C T U R E    ---
    -----------------------------------------------------   
   -- ========================================== 
   -- -- ......... DESIGN UNDER TEST..........--
   -- ==========================================   
    uut: entity work.cdc_phase_scan
        
      generic map (
                period_strobe    => period_strobe_t, 
                insert_reg_input => insert_reg_input_t
                )
             
    port map ( 
          ctrl_reg_i            =>  ctrl_reg_i_t, 
          status_reg_o          =>  status_reg_o_t,
          clk_a_i               =>  clk_a_i_t,  --clk_comp_t,--clk_a_i_t,          
          data_a_i              => data_a_i_t, 
          strobe_a_i            => strobe_a_i_t,        
          strobe_b_i            => strobe_b_i_t,
          clk_b_i               => clk_b_i_t,       
          data_b_o              => data_b_o_t,
          strobe_b_o            => strobe_b_o_t
          ); 
    
    --------------------------------------
    --      TTC PON ONU CLOCK            --
    --------------------------------------
      p_onu_clock_proc: process 
         begin 
          while (enable_clocks = '1') loop
                    clk_a_i_t  <= '1'; 
                 wait for ttc_period/2;  
                   clk_a_i_t  <= '0'; 
                 wait for ttc_period/2; 
                end loop; 
               wait;
       end process;                         
    -----------------------------------------

    --------------------------------------
    --      TTC PON OLT CLOCK            --
    --------------------------------------
    p_olt_clock_proc: process 
       begin 
        while (enable_clocks = '1') loop
            wait for  2.08 ns; 
               while (olt_clock_active = '1') loop
                  clk_b_i_t  <= '1'; 
               wait for ttc_period/2;  
                 clk_b_i_t  <= '0'; 
               wait for ttc_period/2; 
             end loop;    
            end loop; 
             wait;
     end process;                         
-----------------------------------------
  --  Advance  Clock process. 
    p_Comp_clock: process 
          variable  phase_shift : time := 0 ns; 
          begin 
            while (enable_clocks = '1') loop 
              wait until (rising_edge(clk_a_i_t)); 
             --  report "Start_clock"; 
               for i in 0 to 40 loop
                 clk_gen(clk_comp_t,
                           PHASE_o,
                           FREQ,  
                           PHASE,
                           run); 
                end loop; 
              -- report "clock done";
                phase_shift := 300 ps;  -- Comment:  Phase shift 
                PHASE <= phase_shift;
                --
                clk_gen(clk_comp_t,
                           PHASE_o,
                           FREQ,  
                           PHASE,
                           run); 
                 phase_shift := 0 ns; 
                 PHASE <= phase_shift;
                 --
                 for i in 0 to  40  loop
                 clk_gen(clk_comp_t,
                            PHASE_o,
                            FREQ,  
                            PHASE,
                            run); 
                    end loop; 
             end loop;               
          wait; 
    end process; 

    HIGH_TIME<= 0.5 sec/FREQ; 
    --==========================================================
       --- TTC-PON  Data Generator 
       --- =========================================================
     ttcpon_data: if (active_s = true) generate 
       p_ttcpon_gen_data_proc: process 
             variable bit_test : std_logic := '0'; 
             variable  data_toggle  :  std_logic_vector (199 downto 0):= (others => '1');
          begin 
             while (enable_clocks = '1') loop
                wait for  1 ns ;                                               -- Comment:   Waiting  time
                while (olt_clock_active = '1') loop 
                   wait until (rising_edge (clk_a_i_t)); 
                     strobe_a_i_t  <= '1'; 
                       data_toggle := data_toggle+1; 
                     data_a_i_t <= data_toggle ; 
                     for i in 1 to  5 loop 
                       wait until (rising_edge (clk_a_i_t));
                     strobe_a_i_t <= '0'; 
                      end loop;
                 end loop;     
             end loop; 
                 null;
                 report "Simulation End"; 
                 wait; 
           end process; 
      end generate;

  
  --==========================================================
  --- TTC-PON  OLT TX Data Strobe
   --- =========================================================

      p_ttcpon_gen_olt_data_proc: process 
         variable bit_test : std_logic := '0'; 
         variable  data_toggle  :  std_logic_vector (199 downto 0):= (others => '1');
           begin 
             while (enable_clocks = '1') loop
               wait for  24 ns ;                                               -- Comment:   Waiting  time
                 while (olt_clock_active = '1') loop 
                   wait until (rising_edge (clk_b_i_t)); 
                    strobe_b_i_t  <= '1'; 
                   for i in 1 to  5 loop 
                   wait until (rising_edge (clk_b_i_t));
                    strobe_b_i_t <= '0'; 
                   end loop;
                   end loop;     
                   end loop; 
                   null;
                   report "Simulation End"; 
                   wait; 
      end process; 
                   
       --============================================================
       -- == Main process 
       --- ==========================================================
               p_main_proc: process  
                    begin 
                    while (enable_clocks = '1') loop 
                          wait for 73 ns; 
                      for i  in 0 to 3 loop 
                          start_cmd(ctrl_reg_i_t); 
                         wait for 400 ns; 
                          stop_cmd(ctrl_reg_i_t); 
                         wait for 200  ns;
                       end loop; 
                         set_phase_cmd (2, ctrl_reg_i_t); 
                        wait for  500 ns; 
                         set_phase_cmd (3, ctrl_reg_i_t); 
                         wait for 50 ns; 
                         start_cmd(ctrl_reg_i_t);
                         wait for  123 ns ;
                         stop_cmd(ctrl_reg_i_t); 
                         wait for  72  ns;
                         start_cmd(ctrl_reg_i_t);
                         wait for  222 ns ;
                         stop_cmd(ctrl_reg_i_t); 
                        wait for  50 ns;
                         report " SIMULATION  COMPLETE" severity warning; 
                  wait; 
                 end loop; 
               end process; 

   -- ==============================
   -- ==       Spy Process..     ===
   -- ==============================
         p_spy_process: process 
            begin 
                init_signal_spy ("/ipb_slave_tbw/uut/set_generate_p", "set_phase_selected_p", 0); 
                init_signal_spy ("/ipb_slave_tbw/uut/start_aux2_s", "active_phase_scan_s", 0); 
             wait; 
        end process p_spy_process; 
 --        
  -- assert not(set_phase_selected_s = '1' and  strobe_b_o_t = '1' ) report "Something wrong in the set process (overlapping)"  severity FAILURE ; 
 --
 -- ==========================
 -- == Strobe Time checker  == 
 -- ==========================
        p_strobe_time_chkr_proc: process
        variable cc_count_v : integer := 0; 
          begin 
           while (enable_clocks = '1') loop 
              wait until (falling_edge (set_phase_selected_p)) ;
               wait  until rising_edge (strobe_b_o_t);    -- Comment: Aligned. 
                 while (set_phase_selected_p = '0') loop 
                   wait until rising_edge (clk_b_i_t);            
                     if  strobe_b_o_t  = '1' then 
                      meas_strobe_b_o_s <= '1'; 
                      strobe_in_cc_s <= cc_count_v;
                      cc_count_v := 1;
                     else
                      cc_count_v := cc_count_v + 1; 
                     end if;     
                 end loop;    
               meas_strobe_b_o_s <= '0';                     
            end loop; 
            wait; 
        end process;
        p_strobe_assert: process 
           begin 
             while (enable_clocks = '1') loop 
                wait until (rising_edge (meas_strobe_b_o_s));
                wait  until rising_edge (strobe_b_o_t);    -- Comment: Aligned.
                wait  until rising_edge (strobe_b_o_t);   
                  while  (meas_strobe_b_o_s  = '1') loop 
                    wait until (rising_edge (clk_b_i_t)); 
                     assert  (strobe_in_cc_s =  6) report " Something wrong in  shift strobe generator" severity FAILURE; 
                  end loop; 
              end loop;                   
           wait; 
         end process; 
             
    --=============================================================================
    -- ===  relation  between  strobe_b_i and strobe_b_o  checker               === 
    -- ==   The strobe_o  must be shifted  according to  the set_phase_s  value. ===
    --==============================================================================
        set_phase_s <= to_integer(unsigned(ctrl_reg_i_t(0)(5 downto 3)));
          p_strobe_shift_assert: process 
           variable time_strobe_i_v  : time := 0 ns ;
           variable time_strobe_o_v  : time := 0 ns ;
           variable strobe_shift_v    : integer := 0 ;
          begin 
             while (enable_clocks = '1') loop 
               wait until (rising_edge (meas_strobe_b_o_s));
               wait  for 50 ns;
               while  (meas_strobe_b_o_s  = '1') loop 
                wait until (rising_edge (strobe_b_i_t)); 
                   time_strobe_i_v := now;
                   wait until (rising_edge (strobe_b_o_t)) ;
                   time_strobe_o_v := now; 
                   strobe_shift_v := ((time_strobe_o_v - time_strobe_i_v)/ttc_period) -1 ;
                   assert (strobe_shift_v = set_phase_s) report " Something wrong  in  the relation  set_phase and generation shift strobe" severity FAILURE; 
                end loop; 
             end loop;                   
           wait; 
       end process; 
          
     --  Check result of the phase scan procedure. 
      p_check_process:process 
          variable scan_results_v: std_logic_vector ((period_strobe_t*2)-1 downto 0) := (others => '0'); 
          variable result_scan_v: std_logic_vector ((period_strobe_t*2)-1 downto 0) := (others => '0'); 
        begin 
             while (enable_clocks = '1') loop 
                wait until (falling_edge(ctrl_reg_i_t(0)(0))); 
                        result_scan_v  := scan_results_v ;               
                        scan_results_v :=  status_reg_o_t(0)((period_strobe_t*2)-1 downto 0); 
                      if (scan_results_v =  result_scan_v)  then 
                          report "Results"  &  slv2str (scan_results_v); 
                      else
                          report  "New Result" & slv2str(scan_results_v) severity Warning;
                      end if ; 
                  wait until (rising_edge (clk_b_i_t)); 
              end loop; 
              wait;   
        end process p_check_process; 

    --  ============================================================= 
    --- == Check the time of the phase scan is active, must be multiple of 12 cc ==
    --  ============================================================== 
        p_check_time:process
             variable  number_of_cc_v : integer:= 0; 
            variable  result_v : integer  := 0; 
          begin 
             while (enable_clocks = '1') loop 
                 wait until (rising_edge(active_phase_scan_s)); 
                       number_of_cc_v := 0; 
                  while (active_phase_scan_s = '1') loop 
                      number_of_cc_v := number_of_cc_v + 1 ; 
                    wait until (rising_edge (clk_b_i_t)); 
                  end loop;
                  number_of_cc_v := number_of_cc_v -1 ;  
                    result_v := number_of_cc_v rem 12 ; 
                    assert (result_v = 0 )  report "The start stop process is incorrect, must be multiple of 12  cc " severity FAILURE; 
              end loop; 
        end process; 
   


   end ipb_slave_tbw_beha;
 
    