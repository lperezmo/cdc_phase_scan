--===========================================================    
--= Name:        package_tb  -->  IPbus -- CTP- EMu        ==                                       
--= Start Date:  07 - June - 2015                          ==
--= Description: Package auxiliary  for the  test  bench   == 
--===========================================================
library  ieee; 
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all; 
use ieee.numeric_std.all; 
use ieee.std_logic_unsigned.all; 
 
library std; 
use std.textio.all;
-----------------------------------------------------------------------------------------------------
--                     P A C K A G E     D E C L A R A T I O N                                     --
-----------------------------------------------------------------------------------------------------
 package package_tb  is 
-- =================================
-- --     CUSTOM PACKAGE          --
--=================================
 -- use xil_defaultlib.ipbus.all; 
 -- use xil_defaultlib.system_package.all;

 ----------------------------------------------------------------------------------------------------
 --               C L O C K S    S I G N A L                                                       --
 ----------------------------------------------------------------------------------------------------
  constant  SMA_MGT_REFCLK_period    : time := 8.3 ns;    --  Word  Clock  = 120 Mhz
  constant  USER_CLOCK_period        : time := 6.4 ns;    --  User  Clock  = 156 Mhz 
  constant  FRAME_CLOCK              : time := 25  ns;    --  Frame Clock  = 40 Mhz 
  --constant  IPbus_clock            : time := 8 ns;      --  Ipbus Clock  = 125 Mhz   
  constant  IPbus_clock              : time := 13.3 ns;    --  Ipbus Clock  = 32 Mhz   
 ------------------------------------------------------------------------------------------------------
 --                  R E A D    F I L E  S                                                           --
 ------------------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------------------------------------------------
 --                                           W R I T E    F I L E S                                                               --
 function slv2str (slv: std_logic_vector) 
           return  string; 
--========================================================================================================		   
 --                         C O N V E R T   STRING to  STANDARD_LOGIC_VECTOR                             == 
 -- ======================================================================================================= 
  function str2slv (str:string) 
              return  std_logic_vector; 
 -- ========================================================================================================
 -- ==         IPbus   Names  of the SLAVE                                                               === 
-- ========================================================================================================
-- ==                A D V A N C E D   C L O C K   G E N E R A T I O N                                   ==
-- ========================================================================================================
 procedure clk_gen (signal   clk        : out std_logic;
                    signal   PHASE_o    : out time; 
                    constant FREQ       : in real;
                    signal   PHASE      : in time; --:= 0 fs;
                    signal   run        : in std_logic
                   );  
   end;
 --==============================================================f========================================== 
 --                    B O D Y   O F  T H E  P A C K A G E                                               == 
 -- ======================================================================================================= 
package body  package_tb is 

-- ==========================================================================================================
 

	-- ====================================================================
	-- 	    C O N V E R T   S T D__L O G I C_ V E C T O R   T O  H E X   == 
	--      T H E   W I D T H   O F  I N P U T S  ( S L V )              == 
	-- ==================================================================== 
        function  slv2str (slv: std_logic_vector) return  string is 
		           variable  str           :  string ( 1 to (slv'length +3)/4);
				   variable  slv_times4    : std_logic_vector (((slv'length +3 )) /4*4-1 downto 0);
				   variable  str_1         : character; 
				   variable  slv_4         : std_logic_vector (3 downto 0);
				   variable  str_length    : integer; 
				   variable  need_add_bits : integer; 
           begin 
		       str_length    := (slv'length + 3)/4; 
			   need_add_bits := str_length*4-slv'length; 
			   if need_add_bits = 0 then 
			        slv_times4 := slv; 
		       elsif need_add_bits = 1 then 
                    slv_times4 := '0' & slv; 
               elsif need_add_bits = 2  then 
                    slv_times4 := "00" & slv; 
		       else
			        slv_times4  := "000" & slv; 
			end if; 

			for i in 1 to  str_length  loop 
			    slv_4:= slv_times4(slv_times4'length-i*4+3 downto slv_times4'length-i*4);
                case  slv_4 is 
				       when x"0" => str_1:='0'; 
					   when x"1" => str_1:='1'; 
					   when x"2" => str_1:='2'; 
                       when x"3" => str_1:='3'; 
					   when x"4" => str_1:='4';
                       when x"5" => str_1:='5';
                       when x"6" => str_1:='6'; 
                       when x"7" => str_1:='7'; 
                       when x"8" => str_1:='8'; 
                       when x"9" => str_1:='9'; 
                       when x"a" => str_1:='A'; 
                       when x"b" => str_1:='B';
                       when x"c" => str_1:='C'; 
                       when x"d" => str_1:='D'; 
                       when x"e" => str_1:='E'; 
                       when x"f" => str_1:='F'; 
                       when others => str_1:='?'; 
			    end case; 
				   str(i):= str_1; 
			end loop; 
			return  str; 
          end slv2str; 			
	 --======================================================================================
     -- F U N C T I O N   C O N V E R T   S T R I N G   T O   S T D __ L O G I C _V E C T R == 
      -- =====================================================================================
       function  str2slv  (str:  string) return  std_logic_vector is 
       subtype   nib  is std_logic_vector ( 3 downto 0);
          type   nib_table  is array (0 to 15)  of nib; 
	   constant  nib_tbl: nib_table:= ("0000", "0001", "0010", "0011", 
	                                   "0100", "0101", "0110", "0111", 
                                       "1000", "1001", "1010", "1011", 
                                       "1100", "1101", "1110", "1111");
		variable slv           : std_logic_vector ((str'length * 4)-1 downto 0);
		variable pos1          : integer; 
		variable pos_0         : integer; 
		variable pos_9         : integer; 
		variable pos_A         : integer; 
		variable pos_F         : integer; 
		variable pos_sa        : integer; 
		variable pos_sf        : integer; 
		variable b_good_number : boolean;
		variable b_good_letter1: boolean; 
		variable b_good_letter2: boolean; 
		variable b_good_char   : boolean;
		
		begin 
		   slv:= (others => '0');
		     for i in str'range loop 
			    pos1   := character'pos(str(i)); 
				pos_0  := character'pos('0');
				pos_9  := character'pos('9');
				pos_A  := character'pos('A');
				pos_F  := character'pos('F'); 
				pos_sa := character'pos('a');
				pos_sf := character'pos('f');
				
		b_good_number := (pos1 >= pos_0  and pos1 <= pos_9);
		b_good_letter1:= (pos1 >= pos_A  and pos1 <= pos_F);
		b_good_letter2:= (pos1 <= pos_sa and pos1 <= pos_sf);
        b_good_char   := b_good_number or  b_good_letter1 or b_good_letter2;

        assert  b_good_char  report "WRONG NUMBER" severity FAILURE; 

            if b_good_number then 
                 pos1 := pos1 - pos_0;
            elsif b_good_letter1 then 
                 pos1 := pos1 - pos_A + 10; 
            elsif b_good_letter2 then 
                 pos1 := pos1 - pos_sa +10; 
            end if; 
			
			slv(((str'length*4) - (i-1)*4 -1) downto ((str'length *4) - (i-1)*4 -4)) := nib_tbl(pos1); 
		  end loop; 
		 return slv; 
  end str2slv; 
    
      -- ======================================================================================================
      -- ==               A D V A N C E D   C L O C K   P R O C E D U R E                                    ==      
      -- ======================================================================================================
      procedure clk_gen (signal    clk     : out std_logic;
                         signal    PHASE_o : out time;      
                         constant  FREQ    : in  real ;
                         signal   PHASE    : in  time ;
                         signal    run     : in  std_logic)
                is 
                   constant  HIGH_TIME   : time  := 0.5 sec/FREQ;  -- Comment: High time as fixed value 
                   variable  low_time_v  : time;                   -- Comment: Low time calculated per cyecle:
                   variable  cycles_v    : real := 0.0;            -- Comment: Number of cycles 
                   variable  freq_time_v : time := 0 fs;           -- Comment: Time used for generation of cycles 
                  
                   
        begin 
           --Comment: Check the arguments 
            assert (HIGH_TIME /= 0 fs ) report "clk_gen: High time is zero: time resolution to large for frequency" severity FAILURE; 
           
           -----------------------------------  
           -- Comment: Initial phase shift  --
           -----------------------------------
             clk <='0'; 
             PHASE_o <= PHASE; 
             wait for PHASE; 
             
           -----------------------------------
           -- Comment: Generate cycles      --
           ----------------------------------- 
          --for   I in  0 to  10 loop 
               
                if (run = '1')  or (run = 'H') then 
                     clk  <= run; 
               end if; 
               wait for HIGH_TIME; 
             
            ------------------------------------------------------
            -- -- Comment: Low part of cycle                        --
            ------------------------------------------------------
               clk <= '0'; 
               low_time_v := 1 sec * ((cycles_v + 1.0)/ FREQ) - freq_time_v-HIGH_TIME;          -- Comment: + 1.0 for cycle after current 
               wait for low_time_v; 
             
            -------------------------------------------------------
            -- --Comment: Cycle counter and time passed update       --
            -------------------------------------------------------
               cycles_v := cycles_v +1.0; 
              freq_time_v := freq_time_v + HIGH_TIME + low_time_v;
         -- end loop;
   end clk_gen;           
   
  
   
  END PACKAGE BODY  package_tb; 
 