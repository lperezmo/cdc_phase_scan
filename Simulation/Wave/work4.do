onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider ONU
add wave -noupdate /ipb_slave_tbw/uut/data_a_i
add wave -noupdate -divider OLT
add wave -noupdate /ipb_slave_tbw/uut/clk_b_i
add wave -noupdate /ipb_slave_tbw/uut/cntr_period_strobe_s
add wave -noupdate -color Violet /ipb_slave_tbw/uut/strobe_b_i
add wave -noupdate -divider ctp_detect
add wave -noupdate -color Violet -radix binary /ipb_slave_tbw/uut/data_scan_s
add wave -noupdate -color Cyan /ipb_slave_tbw/uut/start_s
add wave -noupdate -color Cyan /ipb_slave_tbw/uut/start_aux_s
add wave -noupdate /ipb_slave_tbw/uut/start_aux2_s
add wave -noupdate /ipb_slave_tbw/ctrl_reg_i_t
add wave -noupdate -color Violet -radix binary /ipb_slave_tbw/uut/shift_strobe_s
add wave -noupdate /ipb_slave_tbw/uut/strobe_to_start_stop_s
add wave -noupdate -color Turquoise /ipb_slave_tbw/uut/set_generate_p
add wave -noupdate /ipb_slave_tbw/uut/indx_phase_s
add wave -noupdate -divider IPbus
add wave -noupdate -radix binary /ipb_slave_tbw/uut/status_reg_o
add wave -noupdate /ipb_slave_tbw/uut/ctrl_reg_i
add wave -noupdate -color Violet /ipb_slave_tbw/uut/indx_phase_s
add wave -noupdate /ipb_slave_tbw/uut/data_scan_s
add wave -noupdate -color Cyan /ipb_slave_tbw/strobe_b_o_t
add wave -noupdate /ipb_slave_tbw/clk_a_i_t
add wave -noupdate /ipb_slave_tbw/uut/toggle_data_s
add wave -noupdate -color {Cornflower Blue} /ipb_slave_tbw/uut/start_stop_a_aux1_s
add wave -noupdate /ipb_slave_tbw/uut/start_stop_a_aux2_s
add wave -noupdate -color {Cornflower Blue} /ipb_slave_tbw/uut/start_stop_a_aux3_s
add wave -noupdate /ipb_slave_tbw/uut/start_stop_a_aux4_s
add wave -noupdate -color Plum -radix binary /ipb_slave_tbw/p_check_process/scan_results_v
add wave -noupdate -color {Light Blue} /ipb_slave_tbw/uut/toggle_data_s
add wave -noupdate -color {Light Blue} /ipb_slave_tbw/uut/data_a_strobe_r
add wave -noupdate /ipb_slave_tbw/uut/data_a_r
add wave -noupdate /ipb_slave_tbw/uut/toggle_data_aux_s
add wave -noupdate -color Violet /ipb_slave_tbw/strobe_a_i_t
add wave -noupdate -color Cyan /ipb_slave_tbw/data_b_o_t
add wave -noupdate /ipb_slave_tbw/uut/strobe_selected_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {3920000 ps} 0} {{Cursor 3} {1531220 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 313
configure wave -valuecolwidth 285
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {227609 ps} {2192263 ps}






run 4500 ns 

WaveRestoreZoom {0 fs} {4500 ns}