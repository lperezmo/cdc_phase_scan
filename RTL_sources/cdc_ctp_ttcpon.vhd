--=============================================================================
--==! @file       :  cdc_ctp_ttcpon                                          ==
--==! @author     :  Luis Alberto Perez Moreno                               ==
--== Start Data :    September 30th 2019                                       ==
--==! @Description:  ONU Phase scan  w.r.t  OLT  Clock  domain                 ==
--==!                It's necessary receive toggle data                        ==
--==!                The scan runs over 2 BC  = 12 OLT tx Strobe               ==
--==!                The module deliver  the result of the Scan and automatic == 
--== !                calculation of a valid position                           ==
--== Simulation :  VHDL ver 2008                                             ==
--=============================================================================
--=============================================================================
--! @file  Phase Scan. 
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all; 
--! Specific packages
use work.ipbus_reg_types.all;  -- IPbus_reg

--============================================================================
--! Entity declaration  Phase Scan, 
--============================================================================
entity cdc_phase_scan is
    generic(
        period_strobe     : integer  := 6;
        insert_reg_input  : boolean  := false; 
        g_user_bits         : integer  := 200 
        );
    port(
        -- IPbus signals 
        ctrl_reg_i        : in   ipb_reg_v(0 downto 0); 
        status_reg_o      : out  ipb_reg_v(0 downto 0); 
        -- 
        clk_a_i           : in   std_logic;
        data_a_i          : in   std_logic_vector;
        strobe_a_i        : in   std_logic;
        -- OLT TX strobe from TTC-PPON user logic 
        strobe_b_i        : in   std_logic;  
        clk_b_i           : in   std_logic;
        -- delayed OLT TX strobe (according to selected delay 1-6)      
        data_b_o          : out  std_logic_vector; 
        strobe_b_o        : out  std_logic
        
        );
        end cdc_phase_scan;
        
--============================================================================
--! Timing in/out
--============================================================================

--============================================================================
--! Architecture declaration for cdc_meso_phase_detect
--============================================================================
 architecture rtl of cdc_phase_scan is
          
   attribute ASYNC_REG : string;
   
   --! Data in / Strobe in register
   signal data_a_r        : std_logic_vector(data_a_i'range);
   signal data_a_strobe_r : std_logic;
   
   --! Data transitory
   signal data_t_r        : std_logic_vector(data_a_i'range);

   --! Start_stop_signal sync to  clock a  
    signal start_stop_a_aux1_s : std_logic := '0'; 
    signal start_stop_a_aux2_s : std_logic := '0'; 

    signal start_stop_a_aux3_s : std_logic := '0'; 
    signal start_stop_a_aux4_s : std_logic := '0'; 
   
   --! Period counter and auxiliary signals. 
   signal  cntr_period_strobe_s : integer range 1 to period_strobe*2;
   signal  data_valid_s : std_logic := '0'; 
   signal  data_scan_s: std_logic_vector (12 downto 1) := (others => '0'); 
   signal  shift_strobe_s: std_logic_vector ((period_strobe*2)-1 downto 0) := (others => '0'); 
   
   --! Constant 
   constant c_data_all_active : std_logic_vector (data_a_i'range) := (others => '1'); 
    signal  toggle_data_aux_s:  std_logic_vector (g_user_bits-1 downto 0):= (others => '0'); 
   signal   toggle_data_s:  std_logic_vector (g_user_bits-1 downto 0):= (others => '0'); 
   
   --! Output  
   signal select_strobe_s : std_logic := '0'; 
   signal data_shift_a_s: std_logic_vector ((g_user_bits*period_strobe)-1 downto 0) := (others => '0'); 
   signal auto_valid_position_s : std_logic_vector (data_scan_s'length downto 1):=(others =>'0');
   
    --!  Start  Process signals
    signal start_s:       std_logic := '0';   
    signal start_aux_s:     std_logic := '0';   
    signal start_aux2_s:    std_logic := '0'; 
    
    --! Set  Process signals 
    signal set_s:          std_logic := '0';   
    signal set_aux_s:      std_logic := '0';   
    signal set_aux2_s:     std_logic := '0';
    signal set_generate_p: std_logic := '0';    -- Comment Pulse  

    signal  set_phase_selected_s : std_logic_vector (2 downto 0) := (others => '0'); 
    signal strobe_to_start_stop_s: std_logic := '0'; 
    signal  strobe_selected_s  : std_logic := '0'; 
    signal indx_phase_s : integer := 0; 
    signal data_b_s : std_logic_vector (data_a_i'range) := (others =>'0'); 
    
    --! Start function. 
     signal start_func_aux2_s:  std_logic := '0'; 
     signal start_func_aux3_s:  std_logic := '0'; 
     signal start_func_p:     std_logic := '0'; 
    
     --! Reset Sync
      signal  reset_sync_p : std_logic := '0';  
      signal  reset_sync_1_s : std_logic := '0'; 
      signal  reset_sync_2_s : std_logic := '0'; 
    
      --! Async Registers definition. 
    attribute async_reg of start_aux_s : signal is "true";
    attribute async_reg of start_stop_a_aux1_s : signal is "true";
       
    attribute async_reg of start_aux2_s :        signal is "true"; 
    attribute async_reg of start_stop_a_aux3_s : signal is "true"; 
    attribute async_reg of data_a2b_r :          signal is "true"; 
    attribute async_reg of strobe_selected_s :   signal is "true"; 

    attribute async_reg of data_scan_s : signal is "true";   -- Comment:  register can receive asynchronous data on the D input pin 
   
  
  -- =======================
   -- ==   FUNCTION        ==
   -- =======================
       --  Comment: 
   function  find_position_func  (data_scan_s: in std_logic_vector) 
    return   std_logic_vector  is 
      variable correct_position_v:  std_logic_vector (data_scan_s'range) := (others => '0');
        begin 
       --   correct_position := (others => '0'); 
           for i in 1 to 10 loop
              if (data_scan_s(i)  = '1'  and  data_scan_s(i+1) = '1' and data_scan_s(i+2) = '1') then 
                  correct_position_v(i+1) := '1';   
               exit; 
              else 
                correct_position_v := correct_position_v; 
            end if;  
             end loop;          
           return correct_position_v; 
   end function find_position_func; 


--============================================================================
-- architecture begin
--============================================================================	
begin

-- =================================================================
-- ==    COMPONENTS  UNDER  clk a   CLOCK   DOMAIN               == 
-- =================================================================

gen_regin : if insert_reg_input generate
process(clk_a_i)
 begin
  if(clk_a_i'event and clk_a_i='1') then
     data_a_strobe_r <= strobe_a_i;
    if start_stop_a_aux2_s  = '1'   or   start_stop_a_aux4_s ='1' then    -- COmment: Activate only during the  Phase Scan Procedure.
     data_a_r        <= toggle_data_s; 
    else 
     data_a_r        <= data_a_i;
     end if; 
   end if;
end process;
end generate;
      
 gen_noregin : if (not insert_reg_input) generate
      data_a_r        <= data_a_i;
      data_a_strobe_r <= strobe_a_i;	
 end generate;

----------------------------------------------------------------------
--    Start stop   clk_b_i  to  clk_a_i                             --
--                                                                  --
--    Single CDC  two  FF  under clk_a_i clock domain               --
--    the start_aux_s signal is stable  12 cc  (clk_b_i)  at least  --
----------------------------------------------------------------------
 p_start_stop_a_process: process (clk_a_i)
     begin 
        if (rising_edge(clk_a_i)) then 
        --
          start_stop_a_aux1_s  <= start_aux_s; 
          start_stop_a_aux2_s  <= start_stop_a_aux1_s; 
        -- 
          start_stop_a_aux3_s  <=  start_aux2_s; 
          start_stop_a_aux4_s  <=  start_stop_a_aux3_s; 
         end if ; 
      end process;
 ------------------------------------------------------------------
 -- Toggle Process:                                              --
 ------------------------------------------------------------------
 p_data_toggle_process: process (clk_a_i)
    variable  toggle_data_aux_v : std_logic_vector (g_user_bits-1 downto 0) := (others => '0'); 
      begin 
         if  (rising_edge (clk_a_i)) then 
          if  start_stop_a_aux2_s  = '1'  or  start_stop_a_aux4_s  = '1' then 
            if  data_a_strobe_r = '1' then 
            toggle_data_s <= not (toggle_data_s); 
            end if; 
         else 
           toggle_data_aux_s <= (others => '0'); 
         end if; 
        end if; 
   end process; 
------------------------------------------------ 
 -- =================================================================
 -- ==    COMPONENTS  UNDER  clk b   CLOCK   DOMAIN               == 
 -- =================================================================
         
  ---------------------------------------------------------------------
   -- Start  process.  Generation of the  active  phase scan period ---
  ---------------------------------------------------------------------
     p_start_stop_proc: process (clk_b_i)
      begin 
         if (rising_edge(clk_b_i)) then 
           if ((strobe_to_start_stop_s = '1')) then  
              start_aux2_s <=  start_aux_s; 
             start_aux_s <= ctrl_reg_i(0)(0);
           end if ; 
        end if; 
      end process; 
--------------------------------------------------------------------
-- Set  process.   Process to Set the phase selected              --
--------------------------------------------------------------------
  p_set_phase_proc: process (clk_b_i)
      begin 
      if (rising_edge(clk_b_i)) then 
          set_aux2_s <=  set_aux_s; 
          set_aux_s  <=  ctrl_reg_i(0)(1);  
        if  set_generate_p = '1' then 
          set_phase_selected_s  <=  ctrl_reg_i(0)(5 downto 3);
        end if; 
      end if; 
   end process; 
       set_generate_p   <= set_aux_s and not set_aux2_s; 

  -------------------------------------------------- -------
  --   Scan Process (Comparison) with  c_data_All_Active 
  ----------------------------------------------------------
    p_scan_process: process (clk_b_i)
    begin 
      if (rising_edge (clk_b_i))then 
            if  (data_a_r = c_data_all_active)  and  start_aux2_s ='1' then 
                   data_valid_s <= '1'; 
            else 
                   data_valid_s <= '0'; 
          end if; 
      end if;   
    end process; 
      data_scan_s(cntr_period_strobe_s) <=  data_valid_s  when (rising_edge (clk_b_i));


  -------------------------------------------------------------------------------------
   --  Start_function.  generate a pulse to calculate the automatic phase. 
   p_start_func_process:process (clk_b_i)
    begin 
       if (rising_edge(clk_b_i)) then 
              start_func_aux3_s  <=  start_func_aux2_s; 
              start_func_aux2_s  <=  start_aux_s;

          if  start_func_p = '1' then 
              auto_valid_position_s  <= find_position_func (data_scan_s); 
         end if; 
       end if; 
    end process;  
    start_func_p <=  start_func_aux3_s  and not start_func_aux2_s; 
  -- ---------------------------------------------------------
  --  Result of the scan                                      
  ------------------------------------------------------------
    status_reg_o(0) <= x"00"&auto_valid_position_s& data_scan_s  when  (start_aux2_s = '1' and (rising_edge (clk_b_i))); 

  ---------------------------------------------------------------  
  -- Counter Process.  (1 to 12)  2 bc
  ----------------------------------------------------------------
    p_cntr_period_strobe : process(clk_b_i)
    begin
      if (rising_edge (clk_b_i))then 
          if (strobe_to_start_stop_s='1')  then
               cntr_period_strobe_s <= 1;
          elsif(cntr_period_strobe_s=(period_strobe*2)) then
             cntr_period_strobe_s <= 1;
           else
              cntr_period_strobe_s <= cntr_period_strobe_s + 1;
          end if;          
        end if;
    end process p_cntr_period_strobe; 
  
    -----------------------------------------------------------
    ---   Reset Process 
    ------------------------------------------------------------
     p_reset_synch: process (clk_b_i)
       begin 
          if (rising_edge (clk_b_i)) then 
            reset_sync_1_s  <=  ctrl_reg_i(0)(2);
            reset_sync_2_s  <= reset_sync_1_s; 
          end if; 
        end process; 
        reset_sync_p  <= reset_sync_1_s and not reset_sync_2_s; 
         
     -------------------------------------------------------------------------
     -- Shift strobe process. 
    ----------------------------------------------------------------------------
       p_shift_strobe_proc:process (clk_b_i)
          begin 
            if (rising_edge(clk_b_i)) then 
               if  reset_sync_p = '1' then 
                   shift_strobe_s <= (others => '0');
               elsif  strobe_to_start_stop_s = '1' then 
                   shift_strobe_s <= "000000000001";  -- Comment: Initial State.
               else 
                   shift_strobe_s <= shift_strobe_s(shift_strobe_s'length-2 downto 0) & strobe_b_i; 
            end if; 
          end if;
        end process; 
        -- Comment Strobe generate every 2 input strobe (strobe_b_i) to  activate the phase scan for 2 bc  
        strobe_to_start_stop_s <= shift_strobe_s(shift_strobe_s'length-1); 
    ----------------------------------------------------------------------------------------

 -- Comment: Position of the phase selected (index to  choose the number of shifts)
 indx_phase_s <= to_integer(unsigned(set_phase_selected_s));

 strobe_selected_s <= strobe_b_i when (indx_phase_s = 0) else 
                    shift_strobe_s(indx_phase_s-1);           -- Comment:  be careful with the indexation value taken from simulation.  

----------------------------------------------------------------------------
--      Output Process                                                    --
-- -------------------------------------------------------------------------
   p_output_proces: process (clk_b_i)
    variable  strobe_reg_v : std_logic := '0'; 
      begin 
          if (rising_edge(clk_b_i)) then 
             if  strobe_selected_s = '1' then 
                   strobe_reg_v :=strobe_selected_s; 
                  strobe_b_o <=  strobe_reg_v;            -- Comment: Aligned to  clk_b_i  and avoiding Latch inference.
                  data_b_o   <= data_a_r; 
              else 
                 strobe_reg_v := '0';
                 strobe_b_o <=  strobe_reg_v;  
              end if; 
          end if; 
      end process;    


 end rtl;

 
                   
