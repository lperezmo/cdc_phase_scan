# CDC phase  scan

Clock domain crossing (CDC) project. In order to find the best position to strobing  ONU data, CDC phase scan module use a phase scan procedure to scan data coming in ONU clock domain (clk_a)  using OLT clock domain (clk_b). The  duration of the scan process is at least  12 clock cycles (2 BC). \
The CDC phase scan module delivers a map with 12  values according to the evaluation of the data in each clock cycle.\
At the end  of the process  we can choose the best position  in a range from 0 to 5.\
The control  is  trough  IPbus interface.

![cdc_phase_scan_result](/Images/cdc-phase-scan-result.png "Result map")

## Folder Structure

 There are the  next  folder structure in the repository:

- `RTL_sources` contains HDL source code.
- `Scripts` contains  a set of tcl and cmd scripts in order to build the project.
- `Simulation` contains  simulation vhd files.
  - `Wave` contains .do files.
- `Documentation` contains description and results presentation.


## Prerequisites

Crafted on Win10 64bit, under QuestaSim-SE 10.3b and 10.4a (Mentor Graphics)

## To Simulate"

 Inside of the  `Scripts`  folder are two ways to built the  simulation project.\

- **`sim_out_libs.tcl`**  built the project  directly in QuestaSim. 

***Note*** before  execution you need to define two  **paths** inside  *sim_out_libs.tcl* file.

```tcl
## =================================================================
##   Absolute data path Set by the user
## ==================================================================
##  Define  PATH TO GIT DIRECTORY
set GIT_DIRECTORY E:/DATA/git_luis/cdc_ctp
```

```tcl
 ##  Libraries  (Precompile libraries).
set LIB_PATH  c:/questalib_2
```



```bash
  cd Scripts
  do  sim_out_libs.tcl
```

- **`sim_quick.cmd`**  built the project  out of QuestaSim automatically (double clicking) - **if the environment variables are corrected defined!**.
  
## Results

 This design was implemented and tested into  [ltu_logic_itsmft](https://gitlab.cern.ch/aliceCTP3/ctp3-fw-ltu_logic_itsmft) project, with successful results.

- **BER Downstream**   10E-15
- **Shift consistency** 1 shift correspond to ~ 4.166 ns (240 Mhz)
- **Full chain downstream latency** (OLT - LTU - CRU)  ~  254 ns 

A detailed description of the results is located in - `Documentation` Folder

## Authors

- **Luis Alberto Perez Moreno** <lperezmo@cern.ch>

See also the list of [contributors](https://gitlab.cern.ch/groups/aliceCTP3/-/group_members) who participated in this project.