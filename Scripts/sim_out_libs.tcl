##**********************************************************************
#***********************************************************************
##**                      Script  information                         **
##**********************************************************************
## Name: master_project.tcl 
##
## Language: TCL (Xilinx VIVADO 2017.4) 
##
## Version: 1.2
##
## Date:  April  - 20th - 2019
##
## Description: Build  cdc phase scan  simulation. 
##  
## ---------------------------------------------------------------------------------------------------
##   F O L D E R    S T R U C T U R E 
##
##      ---+ GIT FOLDER 
##         |
##         +--+-- cdc_ctp
##                |
##                + -- RTL Sources 
##                + -- Scripts 
##                + -- Simulation 
##                + -- work VIVADO 
## ----------------------------------------------------------------------------
##*****************************************************************************
## ============================================================================
##   Absolute data path Set by the user 
## =============================================================================
##  Define  PATH TO GIT DIRECTORY
set GIT_DIRECTORY E:/DATA/git_luis/cdc_ctp 

set GIT_DIRECTORY_2    $GIT_DIRECTORY/Simulation
#  VHDL  Files
set  RTL_PATH          $GIT_DIRECTORY/RTL_sources 
#  Test Bench 
set  SIMULATION_PATH   $GIT_DIRECTORY/Simulation  

#  Libraries  (Precompile libraries). 
set LIB_PATH  c:/Questa_lib
# 

#  Wave Form. 
#set WAVE $GIT_DIRECTORY_2/ICAP_IPb/Simulation/wave



# 
 quit -sim 
  cd   $GIT_DIRECTORY_2/
file mkdir Sim_out_vivado
 cd $GIT_DIRECTORY_2/Sim_out_vivado
# -----------------------------------------------------------------------------
#  Note 1: Before  we need  precomplie  libraries.  ( VIVADO or Quartus). 
# =============================================================================
#  ---------------------------------------------------------------------------------------------------
#  --  VHDL uses logical library names that can be mapped  to Modelsim  library directories.       --
#  --  If libraries are not mapped  properly, and you invoke  your simulation, necessary components -- 
#  --  will not be loaded and simulation will fail.                                                --
#  --------------------------------------------------------------------------------------------------
vmap secureip     $LIB_PATH/secureip
vmap unisim       $LIB_PATH/unisim
vmap unifast      $LIB_PATH/unifast
vmap simprims_ver $LIB_PATH/simprims_ver
vmap unifast_ver  $LIB_PATH/unifast_ver
vmap unimacro     $LIB_PATH/unimacro
vmap unimacro_ver $LIB_PATH/unimacro_ver 
vmap unisims_ver  $LIB_PATH/unisims_ver
#vmap unisims_ver  $LIB_PATH/axi_hwicap_v3_0_19

 #rmdir /s /q lib_work
 #del /q /f vsim.dbg
 #del /q  lib_work

# # Before you can compile your source files, you must create a library in which to store the compilation results.
# ---------------------------------------
#  --    CREATE A LIB CALLED WORK      --
# ---------------------------------------
vlib lib_work  
# ===================================================================================================
# VHDL uses logical library names that can be mapped to ModelSim library directories.              ==
# If libraries are not mapped properly, and you invoke your simulation, necessary components       ==
# will not be loaded and simulation will fail.
vmap work lib_work 

#------------------------------------------------------
#---   P A C K A G E S                              ---
#-------------------------------------------------------
#--  IPbus Packages
vcom -novopt -O0 $RTL_PATH/ipbus_reg_types.vhd


##------------------------------------------------------
#--  VHDL FILES 
#--   Simulation Locally 
#vcom -novopt -O0 $RTL_PATH/slave_icap.vhd
#--  Simulation Global
vcom -novopt -O0 $RTL_PATH/cdc_ctp_ttcpon.vhd




#------------------------------------------------------
#--Compile  Test Bench  and  Packages              --
#-----------------------------------------------------

vcom -novopt -O0 $SIMULATION_PATH/sim_pack.vhd
#---------------------------------------------------
vcom -novopt -O0 $SIMULATION_PATH/ipb_slave_tbw.vhd

vsim  -vopt -t ns -voptargs="+acc"  -debugdb  -L  unisim work.ipb_slave_tbw -do "../Wave/work4.do"
#vsim -fsmdebug -lib work ipb_slave_tbw
view wave
view structure
view signals
log -r /* 

# ------------------------

    