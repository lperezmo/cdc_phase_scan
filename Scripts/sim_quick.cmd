
 cd  ../Simulation
 rmdir /s /q lib_work
 del /q /f vsim.dbg
 del /q  lib_work
vlib lib_work  
vmap work lib_work 
vcom -novopt -O0 ../RTL_sources/ipbus_reg_types.vhd
vcom -novopt -O0 ../RTL_sources/cdc_ctp_ttcpon.vhd
vcom -novopt -O0 sim_pack.vhd
vcom -novopt -O0 ipb_slave_tbw.vhd

vsim  -novopt -t ns -voptargs="+acc"  -fsmdebug -L  unisim work.ipb_slave_tbw -do "Wave/work4.do"
view wave
view structure
view signals
log -r /* 

REM ## ------------------------

    